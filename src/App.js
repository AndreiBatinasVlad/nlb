import { ThemeProvider } from "styled-components";
import { BrowserRouter } from "react-router-dom";

import theme from "./ui/theme";
import GlobalStyles from "./ui/GlobalStyles";
import Routes from "./routes/";

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <Routes />
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
