import styled from "styled-components";

import { Text, Flex } from "../../ui";

const PageContainer = styled.div`
  width: 100%;
  height: 100vh;
  background-image: url("/images/background.png");
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  display: flex;
`;

const MottoText = styled(Text)`
  font-family: "Boomshell";
  width: 0;
  line-height: 1.5;
`;

const PageWithBackground = ({ children }) => (
  <PageContainer>
    <Flex grow>
      <Flex grow="4.5" direction="column">
        <MottoText fontSize="motto">Never look back</MottoText>
      </Flex>

      <Flex grow="1" justifyContent="flex-start">
        {children}
      </Flex>
    </Flex>
  </PageContainer>
);

export default PageWithBackground;
