import React from "react";
import { Route, Switch, Redirect } from "react-router";

import SignUp from "./SignUp";
import SignIn from "./SignIn";
import Products from "./Products";

import PrivateRoute from "./PrivateRoute";

const Routes = () => (
  <Switch>
    <Route exact path="/">
      <Redirect to="/signin" />
    </Route>
    <Route exact path="/signup" component={SignUp} />
    <Route exact path="/signin" component={SignIn} />
    <PrivateRoute exact path="/products" component={Products} />
  </Switch>
);

export default Routes;
