import React from "react";
import PropTypes from "prop-types";
import { Route, Redirect } from "react-router-dom";

import { accessToken } from "../helpers";

const propTypes = {
  // Component to render inside private route (sent like a prop)
  component: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.object,
    PropTypes.func,
  ]),
  // Component to render inside private route (but sent like a child of the PrivateRoute)
  children: PropTypes.object,
};

const PrivateRoute = ({
  component: Component,
  permission,
  children,
  ...rest
}) => {
  if (!accessToken()) {
    return (
      <Redirect
        to={{
          pathname: "/",
        }}
      />
    );
  }

  return (
    <Route
      {...rest}
      render={(props) => (Component ? <Component {...props} /> : children)}
    />
  );
};

PrivateRoute.propTypes = propTypes;

export default PrivateRoute;
