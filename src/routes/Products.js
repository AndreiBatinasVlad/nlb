import { useState } from "react";
import styled from "styled-components";

import { Card, Flex, Section, Text, Button, Loader } from "../ui";

import Header from "./products/Header";
import useProducts from "./products/dataHooks";

const PageContainer = styled.div`
  width: 960px;
  margin: 0 auto;
`;

const ProductsContent = styled.div`
  background-color: ${(props) => props.theme.colors.productsBackground};
  width: 100%;
  height: 100%;
  padding: ${(props) => props.theme.spaces.l};
`;

const ProductImage = styled.img`
  width: 100%;
  height: 200px;
  object-fit: cover;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
`;

const ProductPrice = styled(Section)`
  backgroundcolor: ${(props) => props.theme.colors.darkBackground};
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
`;

const Products = () => {
  const [page, setPage] = useState(1);
  const [products, totalNumberOfRows, loading] = useProducts(page);

  return (
    <>
      <PageContainer>
        <Header />
      </PageContainer>
      <ProductsContent>
        <PageContainer>
          <Flex justifyContent="space-between" wrap="wrap">
            {products.map((product) => (
              <Card width="30%" mB="base" noPadding key={product.id}>
                <ProductImage src={product.imageUrl} />

                <Section pT="xs">
                  <Text mB="base" fontSize="large" bold>
                    {product.title}
                  </Text>
                  <Text>{product.description}</Text>
                </Section>
                <ProductPrice backgroundColor="darkBackground">
                  <Text color="textInverted" fontSize="large" center bold>
                    25.89$
                  </Text>
                </ProductPrice>
              </Card>
            ))}
          </Flex>

          <Flex>
            {loading ? (
              <Loader />
            ) : (
              totalNumberOfRows !== products.length && (
                <Button
                  backgroundColor="greyBackground"
                  color="text"
                  width="170px"
                  p="s"
                  onClick={() => setPage(page + 1)}
                >
                  Load More
                </Button>
              )
            )}

            {!loading && !products.length && <Text>No Entries</Text>}
          </Flex>
        </PageContainer>
      </ProductsContent>
    </>
  );
};
export default Products;
