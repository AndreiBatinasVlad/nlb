import { useEffect, useState } from "react";
import axios from "axios";

import { API_ENDPOINT } from "../../envVars";
import { accessToken } from "../../helpers";

const LIMIT = 12;

const useProducts = (page) => {
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [totalNumberOfRows, setTotalNumberOfRows] = useState(0);

  useEffect(() => {
    setLoading(true);

    const req = axios.create({
      baseURL: `${API_ENDPOINT}products`,
      headers: { Authorization: `Bearer ${accessToken()}` },
      params: {
        limit: LIMIT,
        offset: (page - 1) * LIMIT,
      },
    });

    req()
      .then(function (response) {
        const allProducts = products;
        allProducts.push(...response?.data?.data?.rows);
        setProducts(allProducts);

        setTotalNumberOfRows(response?.data?.data?.count);
        setLoading(false);
      })
      .catch(function (error) {
        console.log(error.response);
        setLoading(false);
      });
  }, [page, products]);

  return [products, totalNumberOfRows, loading];
};

export default useProducts;
