import styled from "styled-components";

import { Flex, Button, Text } from "../../ui/";
import { logout } from "../../helpers";

const HeaderContainer = styled(Flex)`
  position: relative;
`;

const ButtonContainer = styled.div`
  position: absolute;
  right: 0;
`;

const Header = () => (
  <HeaderContainer pV="base" justifyContent="center">
    <Text fontFamily="Boomshell" fontSize="xxlarge">
      Never look back
    </Text>
    <ButtonContainer>
      <Button fontSize="xsmall" p="xs" width="100px" onClick={logout}>
        Log Out
      </Button>
    </ButtonContainer>
  </HeaderContainer>
);

export default Header;
