import React from "react";
import { Formik, Form } from "formik";
import axios from "axios";
import { string, object } from "yup";
import { useHistory, Redirect } from "react-router-dom";

import { Card, Text, Flex, ButtonWithLoader, InputFieldFormik } from "../ui";
import PageWithBackground from "./components/PageWithBackground";

import { API_ENDPOINT } from "../envVars";
import { accessToken } from "../helpers";

const SignUp = () => {
  const history = useHistory();

  if (accessToken()) {
    return (
      <Redirect
        to={{
          pathname: "/products",
        }}
      />
    );
  }

  return (
    <PageWithBackground>
      <Card>
        <Text fontSize="header">Welcome Back!</Text>

        <Formik
          onSubmit={(values, { setSubmitting, setStatus }) => {
            setSubmitting(true);
            setStatus({});

            axios
              .post(`${API_ENDPOINT}signin`, {
                email: values.email,
                password: values.password,
              })
              .then(function (response) {
                localStorage.setItem(
                  "accessToken",
                  response?.data?.data?.accessToken
                );
                setSubmitting(false);
                return history.push("/products");
              })
              .catch(function (error) {
                setSubmitting(false);
                setStatus({
                  error: true,
                  message: error.response.data.message,
                });
              });
          }}
          initialValues={{
            email: "",
            password: "",
            termsAccepted: "",
          }}
          validationSchema={object().shape({
            email: string().email().required("Email is required."),
            password: string().required("No password provided."),
          })}
        >
          {({ isSubmitting, submitForm, status }) => (
            <Form>
              <Flex mV="l" direction="column">
                <InputFieldFormik name="email" placeholder="Email" />

                <InputFieldFormik
                  name="password"
                  placeholder="Password"
                  type="password"
                />
              </Flex>

              {status?.error && status?.message && (
                <Text mV="base" color="error">
                  {status.message}
                </Text>
              )}

              <ButtonWithLoader
                disabled={isSubmitting}
                type="submit"
                onClick={submitForm}
                loading={isSubmitting}
              >
                Login
              </ButtonWithLoader>

              <Text
                underline
                center
                pointer
                mT="l"
                onClick={() => history.push("/signup")}
              >
                New around here? Sign up now
              </Text>
            </Form>
          )}
        </Formik>
      </Card>
    </PageWithBackground>
  );
};

export default SignUp;
