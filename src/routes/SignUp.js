import React from "react";
import { Formik, Form } from "formik";
import { string, object, ref } from "yup";
import { useHistory, Redirect } from "react-router-dom";
import axios from "axios";

import {
  Card,
  Text,
  CheckboxFieldFormik,
  Flex,
  ButtonWithLoader,
  InputFieldFormik,
} from "../ui";
import PageWithBackground from "./components/PageWithBackground";

import { API_ENDPOINT } from "../envVars";
import { accessToken } from "../helpers";

const SignUp = () => {
  const history = useHistory();

  if (accessToken()) {
    return (
      <Redirect
        to={{
          pathname: "/products",
        }}
      />
    );
  }

  return (
    <PageWithBackground>
      <Card>
        <Text fontSize="header">Join Us Now!</Text>

        <Formik
          onSubmit={(values, { setSubmitting, setStatus }) => {
            setSubmitting(true);
            setStatus({});

            axios
              .post(`${API_ENDPOINT}signup`, {
                email: values.email,
                password: values.password,
                repeatPassword: values.passwordConfirmation,
              })
              .then(function (response) {
                localStorage.setItem(
                  "accessToken",
                  response?.data?.data?.accessToken
                );
                setSubmitting(false);
                return history.push("/products");
              })
              .catch(function (error) {
                setSubmitting(false);
                setStatus({
                  error: true,
                  message: error.response.data.message,
                });
              });
          }}
          initialValues={{
            email: "",
            password: "",
            passwordConfirmation: "",
            termsAccepted: "",
          }}
          validationSchema={object().shape({
            email: string().email().required("Email is required."),
            password: string().required("No password provided."),
            passwordConfirmation: string()
              .oneOf([ref("password"), null], "Passwords must match.")
              .required("Password confirmation is required"),
          })}
        >
          {({ isSubmitting, submitForm, status }) => (
            <Form>
              <Flex mV="xl" direction="column">
                <InputFieldFormik name="email" placeholder="Email" />

                <InputFieldFormik
                  name="password"
                  placeholder="Password"
                  type="password"
                />

                <InputFieldFormik
                  name="passwordConfirmation"
                  placeholder="Repeat Password"
                  type="password"
                />

                <Flex justifyContent="flex-start" width="100%">
                  <CheckboxFieldFormik name="termsAccepted">
                    <Flex direction="column" mL="s" alignItems="flex-start">
                      <Text fontSize="xsmall">
                        I agree to no{" "}
                        <Text inline bold fontSize="xsmall">
                          Terms and Conditions
                        </Text>
                      </Text>
                      <Text fontSize="xsmall">
                        and{" "}
                        <Text inline bold fontSize="xsmall">
                          Privacy Policy
                        </Text>
                        .
                      </Text>
                    </Flex>
                  </CheckboxFieldFormik>
                </Flex>
              </Flex>

              {status?.error && status?.message && (
                <Text mV="base" color="error">
                  {status.message}
                </Text>
              )}

              <ButtonWithLoader
                type="submit"
                onClick={submitForm}
                disabled={isSubmitting}
                loading={isSubmitting}
              >
                Create Account
              </ButtonWithLoader>

              <Text
                underline
                center
                pointer
                mT="l"
                onClick={() => history.push("/signin")}
              >
                Already a member? Sign In!
              </Text>
            </Form>
          )}
        </Formik>
      </Card>
    </PageWithBackground>
  );
};

export default SignUp;
