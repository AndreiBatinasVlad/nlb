import styled, { css } from "styled-components";

const Button = styled.button`
  border-radius: 5px;
  width: 100%;
  padding: ${(props) => props.theme.spaces[props.p || "base"]};
  color: ${(props) => props.theme.colors[props.color || "textInverted"]};
  background-color: ${(props) =>
    props.theme.colors[props.backgroundColor || "darkBackground"]};
  font-size: ${(props) => props.theme.fontSizes.large};
  font-weight: 600;
  border: none;

  ${(props) =>
    props.width &&
    css`
      width: ${(props) => props.width};
    `};

  ${(props) =>
    props.fontSize &&
    css`
      font-size: ${(props) => props.theme.fontSizes[props.fontSize]};
    `};

  &:hover {
    outline: none;
    cursor: pointer;
  }

  &:focus {
    outline: none;
  }

  &:disabled {
    background: #000000b5;
  }
`;

export default Button;
