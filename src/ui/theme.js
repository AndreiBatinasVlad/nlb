const spaces = {
  none: "0px",
  xxs: "4px",
  xs: "8px",
  s: "12px",
  base: "16px",
  m: "24px",
  l: "32px",
  xl: "48px",
  xxl: "64px",
};

const fontSizes = {
  motto: "100px",
  header: "30px",
  xxlarge: "20px",
  xlarge: "18px",
  large: "16px",
  base: "14px",
  small: "13px",
  xsmall: "12px",
  xxsmall: "11px",
  h1: "20px",
  h2: "18px",
  h3: "16px",
  h4: "14px",
};

export const colors = {
  transparent: "transparent",
  inherit: "inherit",
  currentColor: "currentColor",

  error: "#FF5252",

  border: "#d5d5d8",

  text: "black",
  textInverted: "#FFF",
  productsBackground: "#f9f9f9",
  inputBackground: "#FAFAFA",
  darkBackground: "black",
  greyBackground: "#ececec",
};

const theme = {
  spaces,
  fontSizes,
  colors,
};

export const spaceTypes = Object.keys(spaces);

export default theme;
