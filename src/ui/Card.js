import styled, { css } from "styled-components";

import { marginMixin } from "../ui/mixins";

const Card = styled.div`
  ${marginMixin};
  padding: ${(props) => props.theme.spaces.l};
  background: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 20px;

  ${(props) =>
    props.noPadding &&
    css`
      padding: 0;
    `};

  ${(props) =>
    props.width &&
    css`
      width: ${props.width};
    `};
`;

export default Card;
