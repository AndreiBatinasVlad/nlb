import { createGlobalStyle, css } from "styled-components";
import { normalize } from "styled-normalize";

const reset = css`
  html {
    font-size: 100%;
  }

  html,
  body,
  div,
  span,
  applet,
  object,
  iframe,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  blockquote,
  pre,
  a,
  abbr,
  acronym,
  address,
  big,
  cite,
  code,
  del,
  dfn,
  em,
  img,
  ins,
  kbd,
  q,
  s,
  samp,
  small,
  strike,
  strong,
  sub,
  sup,
  tt,
  var,
  b,
  u,
  i,
  center,
  dl,
  dt,
  dd,
  ol,
  ul,
  li,
  fieldset,
  form,
  label,
  legend,
  table,
  caption,
  tbody,
  tfoot,
  thead,
  tr,
  th,
  td,
  article,
  aside,
  canvas,
  details,
  embed,
  figure,
  figcaption,
  footer,
  header,
  hgroup,
  main,
  menu,
  nav,
  output,
  ruby,
  section,
  summary,
  time,
  mark,
  audio,
  video {
    margin: 0;
    padding: 0;
    border: 0;
    vertical-align: baseline;
  }
  /* HTML5 display-role reset for older browsers */
  article,
  aside,
  details,
  figcaption,
  figure,
  footer,
  header,
  hgroup,
  main,
  menu,
  nav,
  section {
    display: block;
  }
  /* HTML5 hidden-attribute fix for newer browsers */
  *[hidden] {
    display: none;
  }
  ol,
  ul {
    list-style: none;
  }
  blockquote,
  q {
    quotes: none;
  }
  blockquote:before,
  blockquote:after,
  q:before,
  q:after {
    content: "";
    content: none;
  }
  table {
    border-collapse: collapse;
    border-spacing: 0;
  }
  /* remove default x added to input type search  */
  input[type="search"]::-webkit-search-decoration,
  input[type="search"]::-webkit-search-cancel-button,
  input[type="search"]::-webkit-search-results-button,
  input[type="search"]::-webkit-search-results-decoration {
    -webkit-appearance: none;
  }
`;

const GlobalStyles = createGlobalStyle`
    ${normalize};
    ${reset};

    html {
        box-sizing: border-box;
        min-height: 100%;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
    }

    body {
        font-family: "Geomanist";
    }

    * {
        &,
        &:before,
        &:after {
            box-sizing: inherit;
        }
    }

    address,
    blockquote,
    p,
    pre,
    dl,
    ol,
    ul,
    figure,
    hr,
    table,
    fieldset {
        margin-bottom: ${(props) => props.theme.spaces.base};
    }

    dd,
    ol,
    ul {
        margin-left: ${(props) => props.theme.spaces.base};
    }

    img {
        max-width: 100%;
        max-height: 100%;
        vertical-align: middle;
    }

    p {
        margin-bottom: ${(props) => props.theme.spaces.base};
    }

    hr {
        display: block;
        width: 100%;
        border: 0;
        border-top: ${(props) => props.theme.border};
        padding: 0;
        margin: ${(props) => props.theme.spaces.base} auto;
    }

    .ReactModal__Body--open {
        overflow: hidden;
    }
`;

export default GlobalStyles;
