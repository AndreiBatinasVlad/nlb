import styled, { css } from "styled-components";
import { marginMixin, colorMixin } from "./mixins";

const Text = styled.div`
  ${marginMixin};

  font-size: ${(props) => props.theme.fontSizes.base};
  color: ${(props) => props.theme.colors.text};

  ${colorMixin};

  ${(props) =>
    props.fontSize &&
    css`
      font-size: ${props.theme.fontSizes[props.fontSize]};
    `}

  ${(props) =>
    props.inline &&
    css`
      display: inline;
    `};

  ${(props) =>
    props.center &&
    css`
      text-align: center;
    `}

  ${(props) =>
    props.bold &&
    css`
      font-weight: 600;
    `};

  ${(props) =>
    props.underline &&
    css`
      text-decoration: underline;
    `};

  ${(props) =>
    props.fontFamily &&
    css`
      font-family: ${(props) => props.fontFamily};
    `};

  ${(props) =>
    props.pointer &&
    css`
      &:hover {
        cursor: pointer;
      }
    `};
`;

export default Text;
