import styled from "styled-components";

const Icon = styled.svg`
  fill: none;
  stroke: black;
  stroke-width: 2px;
`;

const HiddenCheckbox = styled.input.attrs({ type: "checkbox" })`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

const StyledCheckbox = styled.label`
  display: inline-block;
  width: 22px;
  height: 22px;
  background: ${(props) => props.theme.colors.inputBackground};
  border-radius: 8px;
  border: 1px solid ${(props) => props.theme.colors.border};
  transition: all 150ms;

  ${HiddenCheckbox}:focus + & {
    box-shadow: 0 0 0 3px pink;
  }

  ${Icon} {
    visibility: ${(props) => (props.checked ? "visible" : "hidden")};
  }
`;

const Wrapper = styled.div`
  display: flex;
  vertical-align: middle;
  input[type="checkbox"] {
    outline: none;
    visibility: hidden;
    position: absolute;
    left: -9999px;
  }

  ${StyledCheckbox} {
    &:hover {
      cursor: pointer;
    }
  }
`;

const Checkbox = ({ checked, children, ...props }) => (
  <Wrapper>
    <HiddenCheckbox checked={checked} {...props} />
    <StyledCheckbox checked={checked} htmlFor={`${props.name}`}>
      <Icon viewBox="0 0 24 24">
        <polyline points="20 6 9 17 4 12" />
      </Icon>
    </StyledCheckbox>
    {children}
  </Wrapper>
);

export default Checkbox;
