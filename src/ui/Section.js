import styled, { css } from "styled-components";

import {
  paddingMixin,
  backgroundMixin,
  colorMixin,
  marginMixin,
} from "./mixins";

const Section = styled.div`
  ${paddingMixin};
  ${(props) => marginMixin(props, false)};
  display: block;
  width: 100%;
  ${backgroundMixin("transparent")};
  ${colorMixin("text")};

  /* width, height, position */
  ${(props) =>
    props.width &&
    css`
      width: ${props.width};
    `};

  ${(props) =>
    props.height &&
    css`
      height: ${props.height};
    `};

  ${(props) =>
    props.position &&
    css`
      position: ${props.position};
    `};
`;

Section.displayName = "Section";

export default Section;
