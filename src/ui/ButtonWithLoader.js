import Button from "./Button";
import Loader from "./Loader";
import Flex from "./Flex";


const ButtonWithLoader = ({ loading, children, ...props }) => (
  <Button {...props}>
    {loading ? (
      <Flex>
        <Loader />{" "}
      </Flex>
    ) : (
      children
    )}
  </Button>
);

export default ButtonWithLoader;
