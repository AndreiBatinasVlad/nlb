import React from "react";
import { useField } from "formik";
import PropTypes from "prop-types";

import Input from "../Input";
import { Section, Text } from "../";

const propTypes = {
  /** The name of the field */
  name: PropTypes.string.isRequired,
};

const InputFieldFormik = ({ marginBottom = "base", ...props }) => {
  const [field, meta] = useField(props);
  return (
    <Section p="none" mB={marginBottom}>
      <Input {...field} {...props} />

      {meta.touched && meta.error ? (
        <Text color="error" fontSize="xxsmall" mT="xxs">
          {meta.error}
        </Text>
      ) : null}
    </Section>
  );
};
InputFieldFormik.propTypes = propTypes;

export default InputFieldFormik;
