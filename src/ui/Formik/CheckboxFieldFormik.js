/*
 * @prettier
 */

import React from "react";
import PropTypes from "prop-types";
import { Field } from "formik";

import Checkbox from "../Checkbox";

const propTypes = {
  /* name of the field */
  name: PropTypes.string.isRequired,
  /** value for this checkbox */
  value: PropTypes.any,
};

const CheckboxFieldFormik = ({ name, value, ...rest }) => {
  return (
    <Field name={name}>
      {({ field, form }) => (
        <Checkbox
          {...field}
          id={name}
          value={value}
          checked={form.values[name]}
          {...rest}
        />
      )}
    </Field>
  );
};

CheckboxFieldFormik.displayName = "CheckboxFieldFormik";

CheckboxFieldFormik.propTypes = propTypes;

export default CheckboxFieldFormik;
