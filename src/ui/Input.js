import styled from "styled-components";

const InputStyled = styled.input`
  display: block;
  width: 280px;
  border-radius: 8px;
  padding: ${(props) => props.theme.spaces.s};
  background: ${(props) => props.theme.colors.inputBackground};
  border: 1px solid ${(props) => props.theme.colors.border};

  &:focus {
    outline: none;
  }
`;

export default InputStyled;
