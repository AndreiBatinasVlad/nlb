import styled, { css } from "styled-components";
import { marginMixin, paddingMixin } from "./mixins";

export const flexContainerMixin = ({
  defaultAlignItems = "center",
  defaultJustifyContent = "center",
}) => css`
  display: flex;
  flex-direction: ${(props) => props.direction || "row"};
  align-items: ${(props) => props.alignItems || defaultAlignItems};
  justify-content: ${(props) => props.justifyContent || defaultJustifyContent};

  ${(props) =>
    props.shrink &&
    css`
      flex-shrink: ${props.shrink === true ? "1" : props.shrink};
    `};

  ${(props) =>
    props.grow &&
    css`
      flex-grow: ${props.grow === true ? "1" : props.grow};
    `};

  ${(props) =>
    props.width &&
    css`
      width: ${props.width};
    `};

  ${(props) =>
    props.height &&
    css`
      height: ${props.height};
    `};

  ${(props) =>
    props.wrap &&
    css`
      flex-wrap: ${props.wrap};
    `}
`;

const Flex = styled.div`
  ${flexContainerMixin};
  ${marginMixin};
  ${(props) => paddingMixin(props, true, "0")}
`;
export default Flex;
