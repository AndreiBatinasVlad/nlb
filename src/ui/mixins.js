import { css } from "styled-components";
import get from "lodash/get";
import PropTypes from "prop-types";

import { spaceTypes } from "./theme";

export const paddingMixinPropTypes = {
  /* String for all paddings */
  p: PropTypes.oneOf(spaceTypes),
  /* Vertical paddings */
  pV: PropTypes.oneOf(spaceTypes),
  /* Horizontal paddings */
  pH: PropTypes.oneOf(spaceTypes),
  /*  Padding Top */
  pT: PropTypes.oneOf(spaceTypes),
  /*  Padding Bottom */
  pB: PropTypes.oneOf(spaceTypes),
  /*  Padding Right */
  pR: PropTypes.oneOf(spaceTypes),
  /*  Padding Left */
  pL: PropTypes.oneOf(spaceTypes),
};

export const paddingMixin = (props, withDefault = true, defaultPadding) => css`
  ${withDefault &&
  css`
    padding: ${defaultPadding || props.theme.spaces.base};
  `}

  ${props.p &&
  css`
    padding: ${props.theme.spaces[props.p]};
  `}


    ${props.pV &&
  css`
    padding-top: ${props.theme.spaces[props.pV]};
    padding-bottom: ${props.theme.spaces[props.pV]};
  `}

    ${props.pH &&
  css`
    padding-left: ${props.theme.spaces[props.pH]};
    padding-right: ${props.theme.spaces[props.pH]};
  `}

    ${props.pT &&
  css`
    padding-top: ${props.theme.spaces[props.pT]};
  `}

    ${props.pB &&
  css`
    padding-bottom: ${props.theme.spaces[props.pB]};
  `};

  ${props.pR &&
  css`
    padding-right: ${props.theme.spaces[props.pR]};
  `};

  ${props.pL &&
  css`
    padding-left: ${props.theme.spaces[props.pL]};
  `}
`;

export const marginMixinPropTypes = {
  /* String for all margins */
  m: PropTypes.oneOf(spaceTypes),
  /* Vertical margins */
  mV: PropTypes.oneOf(spaceTypes),
  /* Horizontal margins */
  mH: PropTypes.oneOf(spaceTypes),
  /*  Margin Top */
  mT: PropTypes.oneOf(spaceTypes),
  /*  Margin Bottom */
  mB: PropTypes.oneOf(spaceTypes),
  /*  Margin Right */
  mR: PropTypes.oneOf(spaceTypes),
  /*  Margin Left */
  mL: PropTypes.oneOf(spaceTypes),
};

export const marginMixin = (props, withDefault = true, defaultMargin) => css`
  ${withDefault &&
  css`
    margin: ${defaultMargin || "0"};
  `};

  ${props.m &&
  css`
    margin: ${props.theme.spaces[props.m]};
  `}

  ${props.mV &&
  css`
    margin-top: ${props.theme.spaces[props.mV]};
    margin-bottom: ${props.theme.spaces[props.mV]};
  `}

    ${props.mH &&
  css`
    margin-left: ${props.theme.spaces[props.mH]};
    margin-right: ${props.theme.spaces[props.mH]};
  `}

    ${props.mT &&
  css`
    margin-top: ${props.theme.spaces[props.mT]};
  `}

    ${props.mB &&
  css`
    margin-bottom: ${props.theme.spaces[props.mB]};
  `}

    ${props.mL &&
  css`
    margin-left: ${props.theme.spaces[props.mL]};
  `}

    ${props.mR &&
  css`
    margin-right: ${props.theme.spaces[props.mR]};
  `}
`;

export const getColor = (props, propName, fallbackColorName) => {
  if (get(props.theme.colors, props[propName])) {
    return props.theme.colors[props[propName]];
  }

  const fallbackColor = get(props.theme.colors, fallbackColorName);
  return fallbackColor || "inherit";
};

export const backgroundMixin = (fallbackColorName = "transparent") => css`
  background-color: ${(props) =>
    getColor(props, "backgroundColor", fallbackColorName)};
`;

export const colorMixin = (fallbackColorName = "inherit") => css`
  color: ${(props) => getColor(props, "color", fallbackColorName)};
`;
