import { createBrowserHistory } from "history";

export const accessToken = () => localStorage.getItem("accessToken");

export const logout = () => {
  localStorage.removeItem("accessToken");

  const history = createBrowserHistory();

  history.go("/signin");
};
